//
//  ApiClient.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 11.05.2022.
//

import Foundation
import Combine

class ApiCLient {
    // MARK: - Variables
    private let baseURL: URL = URL(string: "https://api.openweathermap.org/data/2.5/")!

    // MARK: - Public
    func dataRequestPublisher(router: Router) -> AnyPublisher<Data, Error> {
        URLSession.shared.dataTaskPublisher(for: makeRequest(from: router))
            .tryMap(\.data)
            .eraseToAnyPublisher()
    }

    func dataRequest<T: Decodable>(router: Router) async throws -> T {
        let response: (data: Data, response: URLResponse) = try await URLSession.shared.data(for: makeRequest(from: router))
        return try JSONDecoder().decode(T.self, from: response.data)
    }

    private func makeRequest(from router: Router) -> URLRequest {
        var url: URL = baseURL.appendingPathComponent(router.path)
        if let parameters = router.parameters, !parameters.isEmpty {
            var queryItems: [URLQueryItem] = []
            parameters.forEach { param in
                queryItems.append(.init(name: param.key, value: param.value))
            }
            queryItems.append(.init(name: "appid", value: "e72a7ca9eea7964968cbfc5661d3a890"))
            var urlComponentes = URLComponents(string: baseURL.appendingPathComponent(router.path).absoluteString)
            urlComponentes?.queryItems = queryItems
            urlComponentes?.url.map { urlWithComponents in
                url = urlWithComponents
            }
        }

        print("### url \(url.absoluteString)")

        var request = URLRequest(url: url)
        request.httpMethod = router.method
        return request
    }
}

extension ApiCLient {
    struct Router {
        let path: String
        let method: String
        let parameters: [String: String]?
    }
}
