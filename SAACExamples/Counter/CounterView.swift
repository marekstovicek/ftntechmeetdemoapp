//
//  CounterView.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 13.05.2022.
//

import SwiftUI

struct CounterView: View {

    @State var count: Int = 0

    var body: some View {
        VStack {
            Text("Counter value \(count)")
            Button {
                count += 1
            } label: {
                Text("Increment")
            }
            Spacer()
            InnerCounterView(counterValue: $count)
                .padding()
            Spacer()
        }
        .padding()
        .border(.blue, width: 1)
    }
}

struct InnerCounterView: View {

    @Binding var counterValue: Int

    var body: some View {
        VStack {
            Text("Inner counter")
            Text("Counter value \(counterValue)")
            Button {
                counterValue -= 1
            } label: {
                Text("Decrement")
            }
        }
        .padding()
        .border(.red, width: 1)
    }
}
