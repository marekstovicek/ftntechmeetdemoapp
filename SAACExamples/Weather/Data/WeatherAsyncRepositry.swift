//
//  WeatherAsyncRepositry.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 13.05.2022.
//

import Foundation

class WeatherAsyncRepositry: WeatherAsyncRepositoryType {

    private let apiClient: ApiCLient

    init(apiClient: ApiCLient) {
        self.apiClient = apiClient
    }

    func fetchWeather(for latitude: Double, longitude: Double) async throws -> WeatherDomain {
        try await apiClient
            .dataRequest(
                router: .init(
                    path: "weather",
                    method: "GET",
                    parameters: ["lat": "\(latitude)", "lon": "\(longitude)"]
                )
            )
    }
}
