//
//  WeatherCombineRepository.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 11.05.2022.
//

import Foundation
import Combine

class WeatherCombineRepository: WeatherReactiveRepositoryType {
    private let apiClient: ApiCLient

    init(apiClient: ApiCLient) {
        self.apiClient = apiClient
    }

    func fetchWeather(for latitude: Double, longitude: Double) -> AnyPublisher<WeatherDomain, Error> {
        apiClient
            .dataRequestPublisher(router: .init(path: "weather", method: "GET", parameters: ["lat": "\(latitude)", "lon": "\(longitude)"]))
            .decode(type: WeatherDomain.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
