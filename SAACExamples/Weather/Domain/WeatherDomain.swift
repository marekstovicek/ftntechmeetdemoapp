//
//  WeatherDomain.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 11.05.2022.
//

import Foundation

struct Coordinate: Decodable {
    let lon: Double
    let lat: Double
}

struct Weather: Decodable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct Main: Decodable {
    let temp: Double
    let feels_like: Double
    let temp_min: Double
    let temp_max: Double
    let pressure: Int
    let humidity: Int
}

struct Wind: Decodable {
    let speed: Float
    let deg: Int
}

struct WeatherDomain: Decodable {
    let coord: Coordinate
    let weather: [Weather]
    let base: String
    let main: Main
    let visibility: Int
    let wind: Wind
//      "clouds": {
//        "all": 1
//      },
//      "dt": 1560350645,
//      "sys": {
//        "type": 1,
//        "id": 5122,
//        "message": 0.0139,
//        "country": "US",
//        "sunrise": 1560343627,
//        "sunset": 1560396563
//      },
//      "timezone": -25200,
//      "id": 420006353,
//      "name": "Mountain View",
//      "cod": 200
}
