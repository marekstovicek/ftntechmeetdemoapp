//
//  WeatherRepositoryType.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 11.05.2022.
//

import Foundation
import Combine

protocol WeatherReactiveRepositoryType {
    func fetchWeather(for latitude: Double, longitude: Double) -> AnyPublisher<WeatherDomain, Error>
}

protocol WeatherAsyncRepositoryType {
    func fetchWeather(for latitude: Double, longitude: Double) async throws -> WeatherDomain
}
