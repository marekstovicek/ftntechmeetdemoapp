//
//  WeatherUseCase.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 11.05.2022.
//

import Foundation
import Combine

class WeatherUseCase {

    private let reactiveRepository: WeatherReactiveRepositoryType
    private let asyncRepository: WeatherAsyncRepositoryType?

    init(
        reactiveRepository: WeatherReactiveRepositoryType,
        asyncRepository: WeatherAsyncRepositoryType?
    ) {
        self.reactiveRepository = reactiveRepository
        self.asyncRepository = asyncRepository
    }

    func getWeatherReactive(for latitude: Double, longitude: Double) -> AnyPublisher<WeatherDomain, Error> {
        reactiveRepository
            .fetchWeather(for: latitude, longitude: longitude)
            .eraseToAnyPublisher()
    }

    func getWeatherAsync(for latitude: Double, longitude: Double) async throws -> WeatherDomain? {
        try await asyncRepository?
                    .fetchWeather(for: latitude, longitude: longitude)

    }
}
