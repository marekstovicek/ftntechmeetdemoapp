//
//  AsyncWeatherVM.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 12.05.2022.
//

import Foundation
import Combine
import MapKit

class AsyncWeatherVM: ObservableObject {

    struct Coordinates {
        let latitude: Double
        let longitude: Double
    }

    // MARK: - Variables
    @Published var temperature: String = ""
    @Published var coordinates: MKCoordinateRegion = .init()
    @Published var cities: [City] = City.allCases

    private let useCase: WeatherUseCase

    // MARK: - Initializer
    init(weatherUseCase: WeatherUseCase) {
        self.useCase = weatherUseCase
    }

    func fetchWeather(for city: City) {
        fetchWeather(
            for: .init(
                latitude: city.coordinates.latitude,
                longitude: city.coordinates.longitude
            )
        )
    }

    func fetchWeather(for coordinates: Coordinates?) {
        Task {
            if let data = try? await useCase
                .getWeatherAsync(
                    for: coordinates?.latitude ?? 0,
                    longitude: coordinates?.longitude ?? 0
                ) {
                await processData(data: data)
            }
        }
    }

    @MainActor
    private func processData(data: WeatherDomain) {
        temperature = "Temperature \(celsiusDegreed(from: data.main.temp)) °C"
        coordinates = makeCoordinateRegion(
            latitude: data.coord.lat,
            longitude: data.coord.lon
        )
    }

    private func celsiusDegreed(from kelvin: Double) -> Double {
        (kelvin - 273.15).rounded(.awayFromZero)
    }

    private func makeCoordinateRegion(latitude: Double, longitude: Double) -> MKCoordinateRegion {
        MKCoordinateRegion(
            center: CLLocationCoordinate2D(
                latitude: latitude,
                longitude: longitude
            ),
            span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)
        )
    }
}
