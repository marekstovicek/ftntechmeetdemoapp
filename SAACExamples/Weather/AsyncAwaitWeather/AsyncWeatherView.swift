//
//  AsyncWeatherView.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 13.05.2022.
//

import SwiftUI
import MapKit

struct AsyncWeatherView: View {

    @ObservedObject var vm: AsyncWeatherVM

    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    ForEach(vm.cities) { city in
                        Button {
                            vm.fetchWeather(for: city)
                        } label: {
                            Text(city.title)
                        }
                        .padding()
                    }
                }
                Text(vm.temperature)

                Map(coordinateRegion: .constant(vm.coordinates))
                    .ignoresSafeArea(.all, edges: [.leading, .trailing, .bottom])
            }
        }.navigationTitle("Async / Await")
    }
}
