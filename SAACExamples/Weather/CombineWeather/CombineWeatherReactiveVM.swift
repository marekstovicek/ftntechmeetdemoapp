//
//  CombineWeatherReactiveVM.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 12.05.2022.
//

import Foundation
import Combine
import SwiftUI
import MapKit

class CombineWeatherReactiveVM: ObservableObject {
    // MARK: - Variables
    @Published var temperature: String = ""
    @Published var coordinates: MKCoordinateRegion = .init()
    @Published var cities: [City] = City.allCases
    @Published var iconURL: URL?

    private let useCase: WeatherUseCase
    private var cancelables = Set<AnyCancellable>()

    // MARK: - Initializer
    init(weatherUseCase: WeatherUseCase) {
        self.useCase = weatherUseCase
    }

    func fetchWeather(for city: City) {
        fetchWeather(
            for: .init(
                latitude: city.coordinates.latitude,
                longitude: city.coordinates.longitude
            )
        )
    }

    func fetchWeather(for coordinates: Coordinates?) {
        useCase
            .getWeatherReactive(
                for: coordinates?.latitude ?? 0,
                longitude: coordinates?.longitude ?? 0
            )
            .receive(on: RunLoop.main)
            .sink { completion in
                //
            } receiveValue: { [weak self] data in
                self?.temperature = "Temperature \(self?.celsiusDegreed(from: data.main.temp) ?? 0) °C"
                if let region = self?.makeCoordinateRegion(
                    latitude: data.coord.lat,
                    longitude: data.coord.lon
                ) {
                    self?.coordinates = region
                    self?.iconURL = URL(string: "https://openweathermap.org/img/wn/\(data.weather.first?.icon ?? "")@2x.png")
                }
            }
            .store(in: &cancelables)

    }

    private func celsiusDegreed(from kelvin: Double) -> Double {
        (kelvin - 273.15).rounded(.awayFromZero)
    }

    private func makeCoordinateRegion(latitude: Double, longitude: Double) -> MKCoordinateRegion {
        MKCoordinateRegion(
            center: CLLocationCoordinate2D(
                latitude: latitude,
                longitude: longitude
            ),
            span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)
        )
    }
}

struct Coordinates {
    let latitude: Double
    let longitude: Double
}

enum City: String, CaseIterable, Identifiable {
    case prague
    case zagreb
    case split

    var id: String {
        self.rawValue
    }

    var title: String {
        self.rawValue.capitalized
    }

    var coordinates: Coordinates {
        switch self {
        case .prague:
            return .init(latitude: 50.075539, longitude: 14.437800)
        case .zagreb:
            return .init(latitude: 45.815010, longitude: 15.981919)
        case .split:
            return .init(latitude: 43.508133, longitude: 16.440193)
        }
    }
}
