//
//  CombineWeatherView.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 12.05.2022.
//

import SwiftUI
import MapKit

struct CombineWeatherView: View {

    @ObservedObject var vm: CombineWeatherReactiveVM

    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    ForEach(vm.cities) { city in
                        Button {
                            vm.fetchWeather(for: city)
                        } label: {
                            Text(city.title)
                        }
                        .padding()
                    }
                }
                Text(vm.temperature)
                AsyncImage(url: vm.iconURL) { image in
                    image.resizable()
                } placeholder: {
                    EmptyView()
                }
                .frame(width: 45, height: 45)
                
                Map(coordinateRegion: .constant(vm.coordinates))
                    .ignoresSafeArea(.all, edges: [.leading, .trailing, .bottom])
            }
        }.navigationTitle("Combine")
    }
}
