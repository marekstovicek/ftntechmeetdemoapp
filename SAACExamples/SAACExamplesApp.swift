//
//  SAACExamplesApp.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 11.05.2022.
//

import SwiftUI

@main
struct SAACExamplesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
