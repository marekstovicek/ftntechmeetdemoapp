//
//  ContentView.swift
//  SAACExamples
//
//  Created by Marek Šťovíček on 11.05.2022.
//

import SwiftUI

struct ContentView: View {

    private let apiClient = ApiCLient()

    var body: some View {
        NavigationView {
            VStack {
                NavigationLink {
                    AsyncWeatherView(
                        vm: AsyncWeatherVM(
                            weatherUseCase: WeatherUseCase(
                                reactiveRepository: WeatherCombineRepository(apiClient: apiClient),
                                asyncRepository: WeatherAsyncRepositry(apiClient: apiClient)
                            )
                        )
                    )
                } label: {
                    Text("Async / Await")
                }
                .padding()

                NavigationLink {
                    CombineWeatherView(
                        vm: CombineWeatherReactiveVM(
                            weatherUseCase: WeatherUseCase(
                                reactiveRepository: WeatherCombineRepository(apiClient: apiClient),
                                asyncRepository: WeatherAsyncRepositry(apiClient: apiClient)
                            )
                        )
                    )
                } label: {
                    Text("Combine")
                }
                .padding()

                NavigationLink {
                    CounterView()
                } label: {
                    Text("Property wrappers")
                }
                .padding()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


struct Bla: Codable {
    var test: String
}
